
/*---------------------------------------------------------------------
 *
 * Game.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Game class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include "Dealer.h"

/*------------------------- Implementation ---------------------------*/
 
class Game
{

  public:
    Game();
    Game(bool initial_debug);
    Game(const Game& copy_from);
    Game& operator=(const Game& copy_from);
    ~Game();

    int play(void);
    int doRound(void);
    int processPlayerBet(Player* thisPlayer);
    int dealCard(Player* thisPlayer, bool visible);
    unsigned int checkHand(Player* thisPlayer);
    int printPlayerHand(Player* thisPlayer);
    void queryPlayerDiscard(Player*);
    unsigned int checkStraightFlush(Card** cards);
    unsigned int checkCommonCards(Card** cards);
    bool continuePlaying(void);

    Dealer* getDealer(void) const;

 private:
    bool instructions;
    bool debug;
    Player* currentPlayer;
    Dealer* currentDealer;
};

#endif

