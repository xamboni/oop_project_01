

/*---------------------------------------------------------------------
 *
 * Card.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Card class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Card.h"
#include <iostream>
 
/*------------------------- Implementation ---------------------------*/

/* Card constructor. */
Card::Card(unsigned char initial_suit, unsigned char initial_value)
{
    suit = suit_letters[initial_suit];
    value = initial_value;

    // Faces needed for high cards and ace only.
    if (value == 1)
    {
        face = std::string("A");
    }
    else if (value == 10)
    {
        face = std::string("10");
    }
    else if (value == 11)
    {
        face = std::string("J");
    }
    else if (value == 12)
    {
        face = std::string("Q");
    }
    else if (value == 13)
    {
        face = std::string("K");
    }
    else
    {
        // Turn value into a char.
        char value_string[2] = { (char) (value + 0x30), '\0' };
        face = std::string(value_string);
    }
}

/* Copy constructor */
Card::Card(const Card& copy_from)
{
    suit = copy_from.suit;
    value = copy_from.value;
}

/* Copy assignment */
Card& Card::operator=(const Card& copy_from)
{
    suit = copy_from.suit;
    value = copy_from.value;
    return *this;
}

/* Destructor */
Card::~Card()
{
    // Do nothing.
}

/* Card Getters */
unsigned char Card::getSuit(void) const
{
    if (visible)
    {
        return suit;
    }
    else
    {
        return '?';
    }
}

unsigned char Card::getValue(void) const
{
        return value;
}

std::string Card::getFace(void) const
{
    if (visible)
    {
        return face;
    }
    else
    {
        return "?";
    }
}

/* Card Setters */
unsigned int Card::setSuit(unsigned char new_suit)
{
    if (new_suit > 3)
    {
        // Illegal suit type.
        return 1;
    }
    else
    {
        suit = new_suit;
        return 0;
    }
}

unsigned int Card::setValue(unsigned char new_value)
{
    if (new_value > 13)
    {
        // Illegal suit type.
        return 1;
    }
    else
    {
        value = new_value;
        return 0;
    }
}

unsigned int Card::setVisible(bool newVisible)
{
    visible = newVisible;
    return 0;
}

