
/*---------------------------------------------------------------------
 *
 * Card.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Card class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef CARD_H
#define CARD_H

#include <string>

/*------------------------- Implementation ---------------------------*/
 
class Card
{

  public:
    Card(unsigned char suit, unsigned char value);
    Card(const Card& copy_from);
    Card& operator=(const Card& copy_from);
    ~Card();

    unsigned char getSuit(void) const;
    unsigned char getValue(void) const;
    std::string getFace(void) const;

    unsigned int setSuit(unsigned char new_suit);
    unsigned int setValue(unsigned char new_value);
    unsigned int setVisible(bool newVisible);

 private:
    const char suit_letters[4] = { 'S', 'H', 'D', 'C' };
    char suit;
    unsigned char value;
    std::string face;
    bool visible;
};

#endif

