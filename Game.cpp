

/*---------------------------------------------------------------------
 *
 * Game.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Game class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include <iostream>
#include <stdio.h>
#include <algorithm>

#include "Game.h"

unsigned int getDuplicateCount(unsigned int value, Card** cards);
 
/*---------------------------- Globals -------------------------------*/

unsigned int royalFlushValues[5] = {1, 10, 11, 12, 13};

/*------------------------- Implementation ---------------------------*/

/* Game constructor. */
Game::Game()
{
    currentPlayer = new Player();
    currentDealer = new Dealer();
}

Game::Game(bool initial_debug)
{
    currentPlayer = new Player();
    currentDealer = new Dealer();

    debug = initial_debug;
}

/* Copy constructor */
Game::Game(const Game& copy_from)
{
}

/* Copy assignment */
Game& Game::operator=(const Game& copy_from)
{
    return *this;
}

/* Destructor */
Game::~Game()
{
    delete (currentPlayer);
}

int Game::play(void)
{

    // Set up for the game.
    currentPlayer->addToBank(500);

    bool keepPlaying = true;

    while(keepPlaying)
    {
        keepPlaying = this->doRound();
    }

    return 0;
}

int Game::doRound(void)
{

    currentDealer->getDeck()->resetDeck();
    currentDealer->getDeck()->shuffle();
    unsigned int bet = processPlayerBet(currentPlayer);

    /* Add 5 cards to player hand. */
    for (unsigned int i = 0; i < 5; i += 1)
    {
        dealCard(currentPlayer, true);
    }
//    Card* debugTen = new Card(1, 5);
//    Card* debugJack = new Card(0, 3);
//    Card* debugQueen = new Card(2, 3);
//    Card* debugKing = new Card(3, 3);
//    Card* debugAce = new Card(0, 5);
//    debugTen->setVisible(true);
//    debugJack->setVisible(true);
//    debugQueen->setVisible(true);
//    debugKing->setVisible(true);
//    debugAce->setVisible(true);
//    currentPlayer->getHand()->addCard(debugTen);
//    currentPlayer->getHand()->addCard(debugJack);
//    currentPlayer->getHand()->addCard(debugQueen);
//    currentPlayer->getHand()->addCard(debugKing);
//    currentPlayer->getHand()->addCard(debugAce);

    printPlayerHand(currentPlayer);

    /* Process players keeping cards or discarding. */
    queryPlayerDiscard(currentPlayer);
    printPlayerHand(currentPlayer);

    /* Evaluate if player won. */
    unsigned int multiplier = checkHand(currentPlayer);
    if (multiplier)
    {
        std::cout << "Player wins. Payout is " << multiplier << "-to-1. " << std::endl;
        std::cout << "Adding " << bet * multiplier << " to bank." << std::endl;
        currentPlayer->addToBank(bet * multiplier);
    }

    else
    {
        std::cout << "No winning hand found. Player loses." << std::endl;
    }

    /* Clean player hand. */
    currentPlayer->getHand()->cleanHand();

    unsigned int bank = currentPlayer->getBank();

    bool stopPlaying = false;
    if (bank == 0)
    { 
        // Player our of money. End Game.
        std::cout << "Player out of money." << std::endl;
        stopPlaying = true;
        return stopPlaying;
    }
    else
    {
        // Ask player if they want to keep playing.
        bool keepPlaying = continuePlaying();
        return keepPlaying;
    }
}

int Game::processPlayerBet(Player* thisPlayer)
{
    unsigned int bet = 0;

    std::cout << "Player bank: " << thisPlayer->getBank() << " tokens. Insert token amount: ";
    std::cin >> bet;
    std::cout << "Attempting to bet: " << bet << std::endl;

    // Try this operation. Check result.
    while (thisPlayer->removeFromBank(bet))
    {
        std::cout << "Attempted bet higher than amount in player bank. Place a bet: ";
        std::cin >> bet;
        std::cout << "Attempting to bet: " << bet << std::endl;
    }

    std::cout << "New player bank: " << thisPlayer->getBank() << std::endl;

    return bet;
}

int Game::dealCard(Player* thisPlayer, bool visible)
{
    Card* nextCard = NULL;

    nextCard = currentDealer->getDeck()->getNextCard(visible);

    thisPlayer->getHand()->addCard( nextCard );
    return 0;
}

unsigned int Game::checkHand(Player* thisPlayer)
{

    Card** cards = thisPlayer->getHand()->getCardsInHand();
    // Check each winning hand and break if winner. Return multiplier.o
    unsigned int multiplier = 0;

    // Royal Flush
    multiplier = checkStraightFlush(cards);
    if (multiplier)
    {
        return multiplier;
    }

    multiplier = checkCommonCards(cards);
    if (multiplier)
    {
        return multiplier;
    }

    return multiplier;
}

void Game::queryPlayerDiscard(Player* thisPlayer)
{
    std::cout << "Using 1-5 indicate which cards you would like to discard." << std::endl;
    if (!instructions)
    {
        std::cout << "Example: To discard card 1, card 3, and card 5 input 135." << std::endl;
        instructions = true;
    }

    std::cout << "Input 0 to stay as you are." << std::endl;

    std::string discard;
    std::cin >> discard;

    if (discard != "0")
    {
        for(int i = discard.size() - 1; i >= 0; i -= 1)
        {
            std::string index = discard.substr(i, 1);
            thisPlayer->getHand()->removeCard(std::stoi(index));
        }

        for (unsigned int i = 0; i < discard.size(); i += 1)
        {
            dealCard(currentPlayer, true);
        }
    }
}

int Game::printPlayerHand(Player* thisPlayer)
{
    // Print out player hand.
    unsigned int handCount = thisPlayer->getHand()->getCount();
    Card** playerCards = thisPlayer->getHand()->getCardsInHand();

    for (unsigned int j = 0; j < handCount; j += 1)
    {
        std::string face = playerCards[j]->getFace();
        unsigned int suit = playerCards[j]->getSuit();
//        std::cout << value << suit << " ";
        printf("%s%c ", face.c_str(), suit);
    }

    std::cout << std::endl;

    return 0;
}

// Checking Hands
unsigned int Game::checkStraightFlush(Card** cards)
{
    bool flush = true;
    bool royalFlush = true;
    bool straight = true;

    // Check for flush.
    unsigned int baseSuit = cards[0]->getSuit();
    if (debug) {std::cout << "Checking suits." << std::endl;}
    for (unsigned int i = 0; i < 5; i += 1)
    {
        // Check suits
        if (baseSuit != cards[i]->getSuit())
        {
            flush = false;
            royalFlush = false;
            if (debug) {std::cout << "Player does not have a flush." << std::endl;}
            break;
        }
    }

    unsigned int values[5];
    for (unsigned int i = 0; i < 5; i += 1)
    {
        values[i] = cards[i]->getValue();
    }
    std::sort(values, values + 5);
    
    // Check pattern.
    if (debug) {std::cout << "Checking values." << std::endl;}

    for (unsigned int i = 0; i < 5; i += 1)
    {
        if (values[i] != royalFlushValues[i])
        {
             straight = false;
             royalFlush = false;
             if (debug)
             {
                 std::cout << "Player does not have a royal flush." << std::endl;
             }
             break;
        }
    }

    if (!straight)
    {
        straight = true;
        // Check for straight flush.
        unsigned int start = values[0];
        for (unsigned int i = 1; i < 5; i += 1)
        {
            if ( (start + 1) != values[i])
            {
                if (debug)
                {
                    std::cout << "Player does not have a straight." << std::endl;
                }
                straight = false;
                break;
            }
            else
            {
                start = values[i];
            }
        }
    }

    if (royalFlush)
    {
        return 250;
    }

    else if (straight && flush)
    {
        return 50;
    }

    else if (flush)
    {
        return 6;
    }

    else if (straight)
    {
        return 4;
    }

    else
    {
        return 0;
    }
}

unsigned int Game::checkCommonCards(Card** cards)
{
    if (debug) {std::cout << "Checking common cards." << std::endl;}
    unsigned int multiplier = 0;

    unsigned int values[5];
    for (unsigned int i = 0; i < 5; i += 1)
    {
        values[i] = cards[i]->getValue();
    }
    std::sort(values, values + 5);

    for (unsigned int i = 0; i < 5; i += 1)
    {
        unsigned int duplicates = getDuplicateCount(values[i], cards);
        if (duplicates == 4)
        {
            if (debug) {std::cout << "Four of a kind." << std::endl;}
            multiplier = 25;
            break;
        } 
    }

    if (!multiplier)
    {
        for (unsigned int i = 0; i < 5; i += 1)
        {
            unsigned int duplicates = getDuplicateCount(values[i], cards);
            // Check full house and three of a kind.
            if (duplicates == 3)
            {
                if (debug)
                {
                    std::cout << "Checking three of a kind and full house." << std::endl;
                }
                if (i == 0)
                {
                    unsigned int secondPair;
                    secondPair = getDuplicateCount(values[3], cards);
                    if (secondPair == 2)
                    {
                        if (debug) {std::cout << "Full house." << std::endl;}
                        multiplier = 9;
                        break;
                    }
                }
    
                else if (i == 2)
                {
                    unsigned int secondPair;
                    secondPair = getDuplicateCount(values[0], cards);
                    if (secondPair == 2)
                    {
                        if (debug) {std::cout << "Full house." << std::endl;}
                        multiplier = 9;
                        break;
                    }
                }
    
                if (!multiplier)
                {
                    if (debug) {std::cout << "Three of a kind." << std::endl;}
                    multiplier = 3;
                    break;
                }
            }
        }
    }
 
    if (!multiplier)
    {
        for (unsigned int i = 0; i < 5; i += 1)
        {
            unsigned int duplicates = getDuplicateCount(values[i], cards);
            if (duplicates == 2)
            {
                unsigned int pairValue = values[i];
                if (debug) {std::cout << "Checking Pairs." << std::endl;}
                if (i == 0)
                {
                    for (unsigned int j = 2; j < 4; j += 1)
                    {
                        unsigned int secondPair;
                        secondPair = getDuplicateCount(values[j], cards);
                        if (secondPair == 2)
                        {
                            if (debug) {std::cout << "Two pair." << std::endl;}
                            multiplier = 2;
                            break;
                        }
                    }
                }
    
                else if (i == 1)
                {
                    for (unsigned int j = 3; j < 4; j += 1)
                    {
                        unsigned int secondPair;
                        secondPair = getDuplicateCount(values[j], cards);
                        if (secondPair == 2)
                        {
                            if (debug) {std::cout << "Two pair." << std::endl;}
                            multiplier = 2;
                            break;
                        }
                    }
                }
    
                else if (i == 2)
                {
                    for (unsigned int j = 0; j < 1; j += 1)
                    {
                        unsigned int secondPair;
                        secondPair = getDuplicateCount(values[j], cards);
                        if (secondPair == 2)
                        {
                            if (debug) {std::cout << "Two pair." << std::endl;}
                            multiplier = 2;
                            break;
                        }
                    }
                }
    
                else if (i == 3)
                {
                    for (unsigned int j = 0; j < 2; j += 1)
                    {
                        unsigned int secondPair;
                        secondPair = getDuplicateCount(values[j], cards);
                        if (secondPair == 2)
                        {
                            if (debug) {std::cout << "Two pair." << std::endl;}
                            multiplier = 2;
                            break;
                        }
                    }
                }
    
                if (!multiplier)
                {
                    if (pairValue > 10 || pairValue == 1)
                    {
                        if (debug) {std::cout << "Single pair." << std::endl;}
                        multiplier = 1;
                    }
                }
    
                break;
            }
        }
    }

    return multiplier;
}

bool Game::continuePlaying(void)
{
    char playerAction = '\0';
    bool playerQuit = false;

    std::cout << "Would you like to continue(c) or quit(q)?... ";

    while(true)
    {
        std::cin >> playerAction;

        if (playerAction == 'c')
        {
            return true;
        }
        else if (playerAction == 'q')
        {
            return false;
        }
        else
        {
            std::cout << "Incorrect action. Please choose to hit(h) or stay(s)?... ";
        }
    }
}

/* Game Getters */
Dealer* Game::getDealer(void) const
{
    return currentDealer;
}

/* Game Setters */


/*-------------------------------------------------------------------------- */

unsigned int getDuplicateCount(unsigned int value, Card** cards)
{
    unsigned int values[5];
    for (unsigned int i = 0; i < 5; i += 1)
    {
        values[i] = cards[i]->getValue();
    }
    std::sort(values, values + 5);

    unsigned int duplicates = 0;
    for (unsigned int i = 0; i < 5; i += 1)
    {
        if (value == values[i])
        {
            duplicates += 1;
        }
    }

    return duplicates;
}
