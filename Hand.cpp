

/*---------------------------------------------------------------------
 *
 * Hand.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Hand class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Hand.h"
#include <iostream>
 
/*------------------------- Implementation ---------------------------*/

/* Hand constructor. */
Hand::Hand()
{
    // Start off with space for 5 cards.
    cardsInHand = new Card*[5];
    count = 0;
}

/* Copy constructor */
Hand::Hand(const Hand& copy_from)
{
}

/* Copy assignment */
Hand& Hand::operator=(const Hand& copy_from)
{
    return *this;
}

int Hand::removeCard(unsigned int index)
{

    // Convert from 1 indexed to 0 indexed.
    index -= 1;

    // Move pointers up until the count of cards.
    this->cardsInHand[index] = NULL;

    for(unsigned int i = index; i < count - 1; i += 1)
    {
        this->cardsInHand[i] = this->cardsInHand[i + 1];
    }

    count -= 1;

//    for(unsigned int i = 0; i < count; i += 1)
//    {
//        std::cout << this->cardsInHand[i] << ", ";
//    }
}

/* Destructor */
Hand::~Hand()
{
}

void Hand::cleanHand(void)
{
    for (unsigned int i = 0; i < this->getCount(); i += 1)
    {
        this->cardsInHand[i] = NULL;
    }

    this->setCount(0);
}

/* Hand Getters */
unsigned int Hand::getCount(void) const
{
    return count;
}

Card** Hand::getCardsInHand(void) const
{
    return cardsInHand;
}

/* Hand Setters */
int Hand::addCard(Card* newCard)
{
    cardsInHand[count] = newCard;
    count += 1;
}

void Hand::setCount(unsigned int newCount)
{
    count = newCount;
}
