
/*---------------------------------------------------------------------
 *
 * Hand.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Hand class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef HAND_H
#define HAND_H

#include "Card.h"

/*------------------------- Implementation ---------------------------*/
 
class Hand
{

  public:
    Hand();
    Hand(const Hand& copy_from);
    Hand& operator=(const Hand& copy_from);
    ~Hand();

    int addCard(Card* newCard);
    void cleanHand(void);

    unsigned int getCount(void) const;
    Card** getCardsInHand(void) const;
    int removeCard(unsigned int index);
    int sortHand(void);

    void setCount(unsigned int newCount);

 private:
    unsigned int count;
    Card** cardsInHand;
};

#endif

