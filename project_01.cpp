
/*---------------------------------------------------------------------
 *
 * project_01.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Poker Game
 * 
 * NOTE: Compile: `g++ -g -std=c++11 -o project_01 project_01.cpp Player.cpp Deck.cpp Hand.cpp Card.cpp Game.cpp Dealer.cpp`
 *
 ---------------------------------------------------------------------*/
 
/*---------------------------- Includes ------------------------------*/
 
/* Standard */
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <string>

/* User */
#include "Game.h"
 
/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/
 
/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: 
 * Outputs: 
 * Note: 
 * 
----------------------------------------------------------------------*/
int main (int argc, char* argv[])
{

    int result = 0;

//    Deck* gameDeck = new Deck();

    Game* pokerGame = new Game();
//    Game* pokerGame = new Game(true);
    pokerGame->play();

    delete (pokerGame);
    return result;
}

/*-------------------------------------------------------------------*/

