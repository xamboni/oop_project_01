
/*---------------------------------------------------------------------
 *
 * Deck.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Deck class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef DECK_H
#define DECK_H

#include "Card.h"

/*------------------------- Implementation ---------------------------*/
 
class Deck
{

  public:
    Deck();
    Deck(const Deck& copy_from);
    Deck& operator=(const Deck& copy_from);
    ~Deck();

    int shuffle(void);
    int resetDeck(void);
    Card* getCard(unsigned int index) const;
    Card* getNextCard(bool newVisible);

 private:
    unsigned int deckSize;
    unsigned int cardsLeft;
    unsigned int nextCard;
    Card** deckOfCards;
};

#endif

