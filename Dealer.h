
/*---------------------------------------------------------------------
 *
 * Dealer.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Dealer class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef DEALER_H
#define DEALER_H

#include "Deck.h"
#include "Hand.h"

/*------------------------- Implementation ---------------------------*/
 
class Dealer
{

  public:
    Dealer();
    Dealer(const Dealer& copy_from);
    Dealer& operator=(const Dealer& copy_from);
    ~Dealer();

    Deck* getDeck(void) const;

 private:
    Deck* gameDeck;
};

#endif

