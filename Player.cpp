

/*---------------------------------------------------------------------
 *
 * Player.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Player class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Player.h"
 
/*------------------------- Implementation ---------------------------*/

/* Player constructor. */
Player::Player()
{
    hand = new Hand();
}

/* Copy constructor */
Player::Player(const Player& copy_from)
{
}

/* Copy assignment */
Player& Player::operator=(const Player& copy_from)
{
    return *this;
}

/* Destructor */
Player::~Player()
{

}

int Player::addToBank(unsigned int amount)
{
    bankroll += amount;
    return 0;
}

int Player::removeFromBank(unsigned int amount)
{
    if(amount > bankroll)
    {
        return 1;
    }
    else
    {
        bankroll -= amount;
        return 0;
    }
}

/* Player Getters */
unsigned int Player::getBank(void) const
{
    return bankroll;
}

Hand* Player::getHand(void)
{
    return hand;
}

/* Player Setters */

