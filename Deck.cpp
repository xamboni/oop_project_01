

/*---------------------------------------------------------------------
 *
 * Deck.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Deck class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Deck.h"
#include <iostream>
 
/*------------------------- Implementation ---------------------------*/

/* Deck constructor. */
Deck::Deck()
{
    deckSize = 0;
    deckOfCards = new Card*[52];

    // Create 52 cards on the heap.
    for (int i = 0; i < 4; i += 1)
    {
        // Making j 1-indexed because cards do not have a 0 value.
        for (int j = 1; j < 14; j += 1)
        {
            deckOfCards[(i * 13) + (j - 1)] = new Card(i, j);
            deckSize += 1;
        }
    }

    cardsLeft = 52;
    nextCard = 0;
}

/* Copy constructor */
Deck::Deck(const Deck& copy_from)
{
}

/* Copy assignment */
Deck& Deck::operator=(const Deck& copy_from)
{
    return *this;
}

/* Destructor */
Deck::~Deck()
{
    // Will we ever not have 52 cards on deck?
    for (int i = 0; i < deckSize; i += 1)
    {
        delete (deckOfCards[i]);
    }

    delete (deckOfCards);
}

int Deck::resetDeck(void)
{
    cardsLeft = 52;
    nextCard = 0;
}

int Deck::shuffle(void)
{
//    unsigned int random_array[52] = { 49, 46, 35, 30, 32, 22, 32, 13, 3, 30, 9, 27, 44, 8, 15, 19, 49, 10, 52, 33, 34, 27, 36, 29, 13, 46, 37, 9, 29, 20, 26, 33, 48, 22, 9, 28, 16, 49, 44, 20, 20, 41, 11, 23, 37, 1, 24, 49, 50, 20, 9, 8 };
    //--- Shuffle elements by randomly exchanging each with one other.
    srand (time(NULL));
    for (unsigned int i = 0; i < 52; i += 1)
    {
        int r = rand() % 52;  // generate a random position
        Card* temp = deckOfCards[i];
        deckOfCards[i] = deckOfCards[r];
//        deckOfCards[i] = deckOfCards[random_array[i]];
        deckOfCards[r] = temp;
//        deckOfCards[random_array[i]] = temp;
    }
    nextCard = 0;

    return 0;
}

/* Deck Getters */
Card* Deck::getCard(unsigned int index) const
{
    if (index < deckSize)
    {
        return deckOfCards[index];
    }
    else
    {
        return 0;
    }
}

Card* Deck::getNextCard(bool visible)
{
    Card* returnCard = deckOfCards[nextCard];
    returnCard->setVisible(visible);
    nextCard += 1;
    cardsLeft -= 1; 

    return returnCard;
}

/* Deck Setters */

