
/*---------------------------------------------------------------------
 *
 * Player.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Player class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef PLAYER_H
#define PLAYER_H

#include "Hand.h"

/*------------------------- Implementation ---------------------------*/
 
class Player
{

  public:
    Player();
    Player(const Player& copy_from);
    Player& operator=(const Player& copy_from);
    ~Player();

    int addToBank(unsigned int amount);
    int removeFromBank(unsigned int amount);

    unsigned int getBank(void) const;
    Hand* getHand(void);

 private:
    unsigned int bankroll;
    Hand* hand;
};

#endif

