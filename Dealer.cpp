

/*---------------------------------------------------------------------
 *
 * Dealer.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Dealer class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Dealer.h"
 
/*------------------------- Implementation ---------------------------*/

/* Dealer constructor. */
Dealer::Dealer()
{
    gameDeck = new Deck();
}

/* Copy constructor */
Dealer::Dealer(const Dealer& copy_from)
{
}

/* Copy assignment */
Dealer& Dealer::operator=(const Dealer& copy_from)
{
    return *this;
}

/* Destructor */
Dealer::~Dealer()
{
    delete (gameDeck);
}

/* Dealer Getters */
Deck* Dealer::getDeck(void) const
{
    return gameDeck;
}

/* Dealer Setters */

